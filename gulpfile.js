var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');


gulp.task('sass-cmarp', function(){
	return gulp.src('./src/assets/sass/style.sass')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS())
		.pipe(autoprefixer({
			browsers: ['last 10 versions'],
			cascade: false
		}))
		.pipe(sourcemaps.write('./', {
			includeContent: false,
			sourceRoot: './src/assets/sass/style.sass'
		}))
		.pipe(gulp.dest('./src/assets/css/'))
		.pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('uglify', function(){
	return gulp.src('./src/assets/jsfunctions/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('./src/assets/js/'));
});



gulp.task('watch',function(){
	gulp.watch('./src/assets/sass/*.sass', gulp.series('sass-cmarp'));
	gulp.watch('./src/assets/jsfunctions/*.js', gulp.series('uglify'));
});

gulp.task('default', gulp.parallel('sass-cmarp', 'uglify', 'watch'));