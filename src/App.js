import React, { Component } from 'react';
import './assets/css/style.css';
import './assets/js/mainfunctions.js';
//Components
import Header from './components/header/header';
import Gallery from './components/gallery/gallery';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Gallery />        
      </div>
    );
  }
}

export default App;
