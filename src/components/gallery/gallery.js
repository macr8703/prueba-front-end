import React, { Component } from 'react';
import Slider from "react-slick";
class Gallery extends Component {
  render() {
  	var infogallery = {
  		slide1:{
  			imgd:{
  				background: `url(${require('../../assets/images/banner-aquaman.jpg')})`
  			},
  			imgm:{
  				background: `url(${require('../../assets/images/banner-aquaman-mob.jpg')})`
  			},
  			info:{
  				title:'Aquaman',
  				text: 'Arthur Curry learns that he is the heir of the underwater kingdom of Atlantis. This will become Aquaman, the emperor of Atlantis, committed to defending the entire planet, both on land and in the seas.',
  			}
  		},
  		slide2:{
  			imgd:{
  				background: `url(${require('../../assets/images/banner-bumblebee.jpg')})`
  			},
  			imgm:{
  				background: `url(${require('../../assets/images/banner-bumblebee-mob.jpg')})`
  			},
  			info:{
  				title:'BUMBLEBEE',
  				text: 'In 1987, Charlie, a teenager, finds Bumblebee, very injured, in the junkyard he had reached while fleeing. While he restores it, Charlie perceives that what he has found is not a regular yellow Volkswagen.',
  			}
  		},
  		slide3:{
  			imgd:{
  				background: `url(${require('../../assets/images/banner-venom.png')})`
  			},
  			imgm:{
  				background: `url(${require('../../assets/images/banner-venom-mob.png')})`
  			},
  			info:{
  				title:'Aquaman',
  				text: 'Arthur Curry learns that he is the heir of the underwater kingdom of Atlantis. This will become Aquaman, the emperor of Atlantis, committed to defending the entire planet, both on land and in the seas.',
  			}
  		},
  	};
  	var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div className="main-gallerie">
      	<div className="slide-gallery">
      		<Slider {...settings}>
		        <div>
		        	<div className="slide-show desktop" style={infogallery.slide1.imgd}>
		        		<div className="container">
			          		<div className="info-banner">
				          		<h1>{infogallery.slide1.info.title}</h1>
				          		<p>{infogallery.slide1.info.text}</p>
				          		<div className="links">
				          			<a href="javascript:;" className="yellow-button">Watch Now</a>
				          			<a href="javascript:;" >More info</a>
				          		</div>
				          	</div>
			          	</div>
		        	</div>
		        	<div className="slide-show mobile" style={infogallery.slide1.imgm}>
		        		<div className="container">
			          		<div className="info-banner">
				          		<h1>{infogallery.slide1.info.title}</h1>
				          		<p>{infogallery.slide1.info.text}</p>
				          		<div className="links">
				          			<a href="javascript:;" className="yellow-button">Watch Now</a>
				          			<a href="javascript:;" >More info</a>
				          		</div>
				          	</div>
			          	</div>
		        	</div>
		        </div>
		        <div>
		        	<div className="slide-show desktop" style={infogallery.slide2.imgd}>
		        		<div className="container">
			          		<div className="info-banner">
				          		<h1>{infogallery.slide2.info.title}</h1>
				          		<p>{infogallery.slide2.info.text}</p>
				          		<div className="links">
				          			<a href="javascript:;" className="yellow-button">Watch Now</a>
				          			<a href="javascript:;" >More info</a>
				          		</div>
				          	</div>
			          	</div>
		        	</div>
		        	<div className="slide-show mobile" style={infogallery.slide2.imgm}>
		        		<div className="container">
			          		<div className="info-banner">
				          		<h1>{infogallery.slide2.info.title}</h1>
				          		<p>{infogallery.slide2.info.text}</p>
				          		<div className="links">
				          			<a href="javascript:;" className="yellow-button">Watch Now</a>
				          			<a href="javascript:;" >More info</a>
				          		</div>
				          	</div>
			          	</div>
		        	</div>
		        </div>
		        <div>
		        	<div className="slide-show desktop" style={infogallery.slide3.imgd}>
		        		<div className="container">
			          		<div className="info-banner">
				          		<h1>{infogallery.slide3.info.title}</h1>
				          		<p>{infogallery.slide3.info.text}</p>
				          		<div className="links">
				          			<a href="javascript:;" className="yellow-button">Watch Now</a>
				          			<a href="javascript:;" >More info</a>
				          		</div>
				          	</div>
			          	</div>
		        	</div>
		        	<div className="slide-show mobile" style={infogallery.slide3.imgm}>
		        		<div className="container">
			          		<div className="info-banner">
				          		<h1>{infogallery.slide3.info.title}</h1>
				          		<p>{infogallery.slide3.info.text}</p>
				          		<div className="links">
				          			<a href="javascript:;" className="yellow-button">Watch Now</a>
				          			<a href="javascript:;" >More info</a>
				          		</div>
				          	</div>
			          	</div>
		        	</div>
		        </div>
		      </Slider>
      	</div>
      </div>
    );
  }
}

export default Gallery;
