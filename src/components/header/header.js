import React, { Component } from 'react';
class Header extends Component {
  render() {
    return (
      <header>
      	<div className="container">
      		<div className="row">
      			<div className="logo col-ls-6 col-md-6 col-6 no-padding">
              <img src={require('../../assets/images/logo.png')} className="img-responsive"/>
      			</div>
      			<nav className="col-ls-6 col-md-6 col-6 no-padding">
      				<ul>
      					<li><a href="javascript:;" className="active">Home</a></li>
      					<li><a href="javascript:;" >Categories</a></li>
      					<li><a href="javascript:;" >My Favorites</a></li>
      					<li><a href="javascript:;" >Sign in</a></li>
      				</ul>
      			</nav>
            <div className="hamburger-menu col-6">
              <a href="javascript:;" className="open-menu">abrir</a>
              <a href="javascript:;" className="close-menu">cerrar</a>
            </div>
      		</div>
      	</div>
      </header>
    );
  }
}

export default Header;